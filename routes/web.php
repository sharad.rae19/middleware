<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerOne;
use App\Http\Controllers\ControllerTwo;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::resource('/one',ControllerOne::class);
// Route::post('/logup',[ControllerOne::class,'logup']);

// Route::get('/',[ControllerOne::class,'log']);
// Route::resource('/two',ControllerTwo::class);


Route::group(['Middleware'=>"web"],function(){
    Route::get('/create',[ControllerOne::class,'create']);
    Route::post('/create',[ControllerOne::class,'store']);
});

Route::get('test/{player}/{subplayer}',[ControllerTwo::class,'index']);
Route::get('test/{id}',[ControllerTwo::class,'destroy']);



